{ config, pkgs, lib, ... }:

{
  imports = [
    ../applications/firefox/module.nix
    ../modules/applications/alacritty.nix
    ../modules/applications/mpv.nix
    ../modules/dconf.nix
    ../modules/fonts.nix
    ../modules/gnome-extensions.nix
    ../modules/gtk.nix
    ../modules/mangohud.nix
    ../modules/qt.nix
    ../modules/xresources.nix
    ../templates/module.nix
  ];
}
