{ config, pkgs, lib, ... }:

{
  imports = [
    ../applications/neovim/module.nix
    ../applications/nix/module.nix
    ../applications/tmux/module.nix
    ../applications/zsh/module.nix
    ../modules/applications/aria2.nix
    ../modules/applications/bat.nix
    ../modules/applications/direnv.nix
    ../modules/applications/git.nix
    ../modules/applications/man.nix
    ../modules/applications/skim.nix
    ../modules/applications/ssh.nix
    ../modules/base16.nix
    ../modules/defaults.nix
    ../modules/email.nix
    ../modules/home.nix
    ../modules/nixpkgs.nix
  ];
}
