{
  desktop = ./desktop.nix;
  extra = ./extra.nix;
  gaming = ./gaming.nix;
  terminal = ./terminal.nix;
}

