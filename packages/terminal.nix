{ pkgs, ... }:

{
  home.packages = with pkgs; [
    exa
    gitui
    git-secret
    ripgrep
    ripgrep-all
    tealdeer
    fd
    sd
    bottom
    thefuck
    termshark
    du-dust
    bviplus
  ];
}
