{ pkgs, ... }:

{
  home.packages = with pkgs; [
    zotero
    viber
    cura
    wireshark

    gimp-with-plugins
    inkscape
    krita
    xournalpp
  ];
}
