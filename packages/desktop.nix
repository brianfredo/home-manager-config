{ pkgs, ... }:

{
  home.packages = with pkgs; [
    fragments
    gtg
    lollypop
    wike
    qalculate-gtk
    pika-backup
    clapper
    icon-library
  ];
}
