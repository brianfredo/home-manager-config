{ pkgs, polymc, ... }:

{
  home.packages = with pkgs; [
    steam
    polymc
    openrgb
  ];
}
