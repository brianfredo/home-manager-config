# home-manager-config
# Copyright (C) 2021  Jacob Gogichaishvili
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version

{
  description = "TheOPtimal's home-manager configurations";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";

    nixpkgs.url = "github:NixOS/nixpkgs/release-21.11";

    home-manager = {
      url = "github:nix-community/home-manager/release-21.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    opkgs = {
      url = "gitlab:TheOPtimal/opkgs";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };

    polymc.url = "github:PolyMC/PolyMC";

    # Firefox userChrome.css
    firefox-gnome-theme = {
      url = "github:rafaelmardojai/firefox-gnome-theme";
      flake = false;
    };

    # base16 related
    base16 = {
      url = "github:SenchoPens/base16.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    base16-nord = {
      url = "github:spejamchr/base16-nord-scheme";
      flake = false;
    };

    base16-shell = {
      url = "github:chriskempson/base16-shell";
      flake = false;
    };

    base16-tmux = {
      url = "github:mattdavis90/base16-tmux";
      flake = false;
    };

    base16-zathura = {
      url = "github:HaoZeke/base16-zathura";
      flake = false;
    };
  };

  outputs = { self, flake-utils, nixpkgs, home-manager, opkgs, polymc, base16, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system: let
      # Nixpkgs
      pkgs = nixpkgs.legacyPackages.${system};
      # Home manager packages
      hm-packages = home-manager.packages.${system};

      # Extra arguments provided to the modules
      extraSpecialArgs = inputs // {
        polymc = polymc.packages.${system}.polymc;
        opkgs  = opkgs.legacyPackages.${system};
        olib   = opkgs.lib;
      };
      # Out-of-tree modules
      extraModules = [ base16.homeManagerModule ];
    in rec {
      homeManagerConfigurations = let
        # Simple function to reduce boilerplate
        mkBasicConfig = attrs: home-manager.lib.homeManagerConfiguration ({
          inherit pkgs system extraSpecialArgs extraModules;
        } // attrs);

        # This expands on the above function but is purpose-built for desktops only.
        mkDesktopConfig = configPath: mkBasicConfig {
          configuration = configPath;
          username = "optimal";
          homeDirectory = "/home/optimal";
        };

        # Map `mkDesktopConfig` to each imported confguration
      in (builtins.mapAttrs (_: config: mkDesktopConfig config) self.hmConfigsRaw) // {
        # Add extra configuration for nix-on-droid as it has different requirements
        nix-on-droid = mkBasicConfig {
          configuration = ./home-cli.nix;
          username = "nix-on-droid";
          homeDirectory = "/data/data/com.termux.nix/files/home";
        };
      };

      # NixOS modules to easily add these configs to a NixOS system
      nixosModules = let
        # This function generates a NixOS module that realizes the configuration.
        mkHomeModule = config: {
          home-manager = {
            # useGlobalPkgs = true;
            useUserPackages = true;
            users.optimal = config;

            # Optionally, use home-manager.extraSpecialArgs to pass
            # arguments to home.nix
            inherit extraSpecialArgs;
            sharedModules = extraModules;
          };
        };
        # This generates a NixOS module for each imported home-manager configuration
      in builtins.mapAttrs (_: config: mkHomeModule config) self.hmConfigsRaw;

      devShell = pkgs.mkShell {
        packages = [
          hm-packages.home-manager
        ];
      };

      # Activation packages, running one applies the home-manager configuration. An activationPackage is mapped to each home-manager configuration.
      packages = builtins.mapAttrs (_: config: config.activationPackage) homeManagerConfigurations;
      apps = builtins.mapAttrs (_: pkg: { type = "app"; program = "${pkg}/activate"; }) packages;
    }) // {
      # Raw imports of the configurations
      hmConfigsRaw = {
        desktop = import ./home-desktop.nix;
        desktop-minimal = import ./home-desktop-minimal.nix;
        cli = import ./home-cli.nix;
      };

      overlays = import ./overlays.nix;
    };
}
