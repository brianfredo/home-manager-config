{ config, pkgs, lib, ... }:

rec {
  home.packages = with pkgs.gnomeExtensions; [
    blur-my-shell
    clipboard-indicator
    gsconnect
    removable-drive-menu
    desktop-cube
  ];

  # Enable installed extensions
  dconf.settings."org/gnome/shell".enabled-extensions = [
    "screen-autorotate@kosmospredanie.yandex.ru"
  ] ++ map (extension: extension.extensionUuid) home.packages;
  dconf.settings."org/gnome/shell".disabled-extensions = [];

  # Configure blur-my-shell
  dconf.settings."org/gnome/shell/extensions/blur-my-shell" = {
    appfolder-dialog-opacity = 0.5;
    blur-dash = true;
    brightness = 0.85;
    dash-opacity = 0.25;
    sigma = 15; # Sigma means blur amount
    static-blur = true;
  };

  # Configure GSConnect
  dconf.settings."org/gnome/shell/extensions/gsconnect".show-indicators = true;
}
