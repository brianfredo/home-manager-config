{ config, pkgs, lib, ... }:

{
  programs.git = {
    enable = true;
    package = pkgs.gitAndTools.gitFull;
    # Configure identity
    userName = config.accounts.email.accounts.Primary.realName;
    userEmail = config.accounts.email.accounts.Primary.address;
    # Enable git LFS
    lfs.enable = true;
    # Use Git Delta for diffs
    delta = {
      enable = true;
      options = {
        decorations = {
          commit-decoration-style = "bold yellow box ul";
          file-decoration-style = "none";
          file-style = "bold yellow ul";
        };
        features = "decorations";
        whitespace-error-style = "22 reverse";
      };
    };
  };
}
