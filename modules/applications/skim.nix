{ config, pkgs, lib, ... }:

{
  programs.skim = {
    enable = true;
    enableZshIntegration = true;
  };
}
