{ config, pkgs, lib, ... }:

{
  programs.ssh = {
    enable = true;
    matchBlocks = {
      gitlab = {
        user = "git";
        hostname = "gitlab.com";
        port = 22;
        identityFile = "~/.ssh/id_rsa";
        identitiesOnly = true;
      };
      github = {
        user = "git";
        hostname = "github.com";
        port = 22;
        identityFile = "~/.ssh/id_rsa";
        identitiesOnly = true;
      };

      optimal-pc = {
        user = "optimal";
        hostname = "192.168.100.100";
        port = 22;
        identityFile = "~/.ssh/id_rsa";
        identitiesOnly = true;
      };
      optimalbook = {
        user = "optimal";
        hostname = "192.168.100.50";
        port = 22;
        identityFile = "~/.ssh/id_rsa";
        identitiesOnly = true;
      };
    };
  };
}
