{ config, pkgs, lib, ... }:

{
  dconf.settings = {
    "org/gnome/desktop/background".picture-uri = "file://${pkgs.nixos-artwork.wallpapers.nineish.gnomeFilePath}";
    "org/gnome/desktop/background".picture-uri-dark = "file://${pkgs.nixos-artwork.wallpapers.nineish-dark-gray.gnomeFilePath}";
  };
}
