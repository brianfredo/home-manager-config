{ config, pkgs, lib, ... }:

{
  gtk = {
    enable = true;

    font = {
      name = config.fonts.default.font;
      package = config.fonts.default.package;
      size = config.fonts.size.body;
    };

    theme = {
      package = pkgs.gnome.gnome-themes-extra;
      name = "Adwaita-dark";
    };
  };
}
