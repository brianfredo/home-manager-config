{ config, pkgs, opkgs, lib, ... }:

let
  fontModule = fontName: fontPackage: lib.types.submodule ({ config, ... }: {
    options = {
      font = lib.mkOption {
    type = lib.types.str;
        default = fontName;
        description = "What font within the font family of the font package to use.";
      };
      package = lib.mkOption {
        type = lib.types.package;
        default = fontPackage;
        description = "Which font package to use.";
      };
    };
  });
  header = level: defSize: lib.mkOption {
    type = lib.types.int;
    default = defSize;
    description = "Font size for level ${level} headers.";
  };
  headersModule = lib.types.submodule ({ config, ... }: {
    options = {
      levelOne   = header 1 24;
      levelTwo   = header 2 20;
      levelThree = header 3 18;
      levelFour  = header 4 16;
      levelFive  = header 5 14;
    };
  });
  sizeModule = lib.types.submodule ({ config, ... }: {
    options = {
      headers = lib.mkOption {
        type = headersModule;
        default = {};
        description = "Options related to header font size configuration";
      };
      body = lib.mkOption {
        type = lib.types.int;
        default = 12;
        description = "Body (base) font size.";
      };
      monospace = lib.mkOption {
        type = lib.types.int;
        default = 10;
        description = "Monospace (terminal) font size.";
      };
    };
  });
in {
  options.fonts = {
    default = lib.mkOption {
      type = fontModule "Noto Sans" pkgs.noto-fonts;
      default = {};
      description = "Options related to default font configuration";
    };
    serif = lib.mkOption {
      type = fontModule "DejaVu Serif" pkgs.dejavu_fonts;
      default = {};
      description = "Options related to serif font configuration";
    };
    monospace = lib.mkOption {
      type = fontModule "FiraCode Nerd Font" opkgs.fonts.firacode-nf;
      default = {};
      description = "Options related to monospace font configuration";
    };
    size = lib.mkOption {
      type = sizeModule;
      default = {};
      description = "Options related to font size configuration";
    };
  };

  config.fonts = {
    default.font = "Cantarell";
    default.package = pkgs.cantarell-fonts;

    serif.font = "DejaVu Serif";
    serif.package = pkgs.dejavu_fonts;

    monospace.font = "FiraCode Nerd Font";
    monospace.package = opkgs.fonts.firacode-nf;

    size = {
      headers = {
        levelOne   = 24;
        levelTwo   = 20;
        levelThree = 18;
        levelFour  = 16;
        levelFive  = 14;
      };
      body = 12;
      monospace = 10;
    };
  };

  config.home.packages = [
    config.fonts.default.package
    config.fonts.serif.package
    config.fonts.monospace.package
  ];
}
