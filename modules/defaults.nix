{ config, pkgs, lib, ... }:

{
  options.defaultApps = {
    browser = lib.mkOption {
      type = lib.types.str;
      default = "${pkgs.lynx}/bin/${pkgs.lynx.meta.mainProgram}";
      defaultText = "\${pkgs.lynx}/bin/\${pkgs.lynx.meta.mainProgram}";
      description = "Path to the default web browser.";
    };
    terminal = lib.mkOption {
      type = lib.types.str;
      default = "${pkgs.xterm}/bin/${pkgs.xterm.meta.mainProgram}";
      defaultText = "\${pkgs.xterm}/bin/\${pkgs.xterm.meta.mainProgram}";
      description = "Path to the default terminal emulator.";
    };
    editor = lib.mkOption {
      type = lib.types.str;
      default = "${pkgs.vim}/bin/${pkgs.vim.meta.mainProgram}";
      defaultText = "\${pkgs.xterm}/bin/\${pkgs.vim.meta.mainProgram}";
      description = "Path to the default text editor.";
    };
  };

  config.defaultApps = {
    browser = "${config.programs.firefox.package}/bin/${config.programs.firefox.package.meta.mainProgram}";
    terminal = "${config.programs.alacritty.package}/bin/${config.programs.alacritty.package.meta.mainProgram}";
    editor = "${config.programs.neovim.package}/bin/${config.programs.neovim.package.meta.mainProgram}";
  };
}
