{ config, pkgs, lib, base16-nord, ... }:

{
  config.scheme = "${base16-nord}/nord.yaml";
}
