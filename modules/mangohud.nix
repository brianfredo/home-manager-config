{ config, pkgs, lib, ... }:

{
  programs.mangohud = {
    enable = true;
    enableSessionWide = true;
    settingsPerApplication = {
      mpv = {
        no_display = true;
      };
    };
  };
}
