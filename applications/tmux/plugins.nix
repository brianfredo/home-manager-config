{ lib

, tmuxPlugins
, fetchFromGitHub

, xsel
, wl-clipboard
}:

{
  base16-statusline = tmuxPlugins.mkTmuxPlugin {
    pluginName = "tmux-base16-statusline";
    version = "unstable-2019-5-19";
    src = fetchFromGitHub {
      owner = "jatap";
      repo = "tmux-base16-statusline";
      rev = "5dc655dfcfa3fcb574c28fb2b8c57bdd29b178cf";
      sha256 = "Z3JEQZegpyeyKz1uazDCCF50rc/QqW2LCrMRFo+SQIg=";
      fetchSubmodules = true;
    };
  };
  vim-tmux-resizer = tmuxPlugins.mkTmuxPlugin {
    pluginName = "vim-tmux-resizer";
    rtpFilePath = "vim-tmux-resizer.tmux";
    version = "unstable-2021-9-9";
    src = fetchFromGitHub {
      owner = "TheOPtimal";
      repo = "vim-tmux-resizer";
      rev = "446f60f8e204b4935261d87e6cec7f9d33472d61";
      sha256 = "WTx4re3Spce4Ylsyj4iOLKsjTMF8lNR0BpVNS8Qq49U=";
    };
    meta = with lib; {
      homepage = "https://github.com/TheOPtimal/vim-tmux-resizer";
      description = "Seamless pane resizing between tmux panes and vim splits";
      license = licenses.mit;
    };
  };
  yank = tmuxPlugins.yank.overrideAttrs (oldAttrs: rec {
    nativeBuildInputs = [ xsel wl-clipboard ];
  });
}
