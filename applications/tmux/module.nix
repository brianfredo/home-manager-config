{ config, pkgs, lib, olib, base16-tmux, ... }:

let
  customPackages = pkgs.callPackage ./plugins.nix {};
in {
  programs.tmux = {
    enable = true;
    # Resize the tmux session to the size of the smallest window
    aggressiveResize = true;
    # Use a 24 hour clock
    clock24 = true;
    # Use vi bindings
    keyMode = "vi";
    # Use C-a as the prefix key
    shortcut = "a";
    # Set zsh as the shell
    shell = "${pkgs.zsh}/bin/zsh";
    # Other configuration
    extraConfig = olib.concatFiles [ (config.scheme base16-tmux) ./tmux.conf ];
    # Plugins
    plugins = with pkgs.tmuxPlugins; with customPackages; [
      {
        plugin = base16-statusline;
        extraConfig = builtins.readFile ./pluginConfig/base16-statusline.conf;
      }
      open
      yank
      prefix-highlight
      vim-tmux-navigator
      vim-tmux-resizer
    ];
  };
}
