{ config, pkgs, lib, ... }:

{
  # Symlink nix.conf and configure nix
  home.file.".config/nix/nix.conf".source = ./nix.conf;

  # Nixpkgs

  # Configure nixpkgs internally
  nixpkgs = {
    config = import ./config.nix;
  };
  # Add symlinks and configure nixpkgs externally
  xdg.configFile = {
    "nixpkgs/config.nix".source = ./config.nix;
  };
}
