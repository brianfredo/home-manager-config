eval `thefuck -a`

# Setopt
setopt interactive_comments

# Autoload
unalias run-help
autoload -U colors zcalc zmv run-help
colors

# Window Title
function set_win_title(){
    echo -ne "\033]0; $(pwd | sed "s:$HOME:~:g") \007"
}

# pidswallow
[ -n "$DISPLAY" ]  && command -v xdo >/dev/null 2>&1 && xdo id > /tmp/term-wid-"$$"
trap "( rm -f /tmp/term-wid-"$$" )" EXIT HUP

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.config/zsh/p10k.zsh ]] || source ~/.config/zsh/p10k.zsh
