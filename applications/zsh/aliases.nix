{
  shellAliases = {
    c    = "clear";                     # Simple alias for clear
    mv   = "zmv";                       # Better ls
    cp   = "cp -i";                     # Confirm before overwriting something
    df   = "df -h";                     # Human-readable sizes
    free = "free -m";                   # Show sizes in MB
    ls   = "exa --icons";               # Better LS
    la   = "ls -a";                     # Alias for seeing dotfiles
    ll   = "ls -l";                     # Alias for detailed file listing
    lt   = "ls -T";                     # Tree-style
    le   = "ls -@";                     # Alias for seeing extended file attributes
    l    = "ls -la@";                   # D) All of the above

    sudo = "doas";                      # Sudo is unironically a bloated mess for desktop use.
    fzf     = "sk";                     # sk is better than fzf.
    neovide = "\\neovide --multiGrid";  # Add some options to neovide

    # Git aliases
    ga   = "git add";
    gaa  = "git add .";
    gc   = "git commit -m";
    gp   = "git push";
    gpo  = "git push origin";
    gpom = "git push origin master";
  };
  shellGlobalAliases = {
    # Folder aliases
    "..."         = "../..";
    "...."        = "../../..";
    "....."       = "../../../..";
    "......"      = "../../../../..";
    "......."     = "../../../../../..";
    "........"    = "../../../../../../..";
    "........."   = "../../../../../../../..";
    ".........."  = "../../../../../../../../..";
    "..........." = "../../../../../../../../../..";
  };
}
