{ config, pkgs, opkgs, lib, firefox-gnome-theme, ... }:

let
  _firefox = if lib.versionAtLeast config.home.stateVersion "19.09" then
    pkgs.firefox
  else
    pkgs.firefox-unwrapped;

  firefox = (_firefox.overrideAttrs (old: {
    buildInputs = with pkgs; old.buildInputs ++ [ makeWrapper ];
    postInstall = ''
      wrapProgram $out/bin/firefox --set MOZ_USE_XINPUT2 1
    '';
  })).override {
    cfg = {
      # Enable GNOME integration
      enableGnomeExtensions = true;
      # Enable native tridactyl host
      enableTridactylNative = true;
    };
  };
in {
  imports = [
    # Configure tridactyl extension
    ./tridactyl/module.nix
  ];

  programs.firefox = {
    enable = true;
    package = firefox;
    # Set extensions
    extensions = builtins.filter (val: builtins.typeOf val == "set" && builtins.hasAttr "type" val && val.type == "derivation") (builtins.attrValues opkgs.firefoxAddons);
    profiles = {
      # Default profile
      preferred = {
        name = "preffered";
        isDefault = true;
        userChrome = ''
          @import "${firefox-gnome-theme}/userChrome.css";
        '';
        settings = let
          searchEngine = "DuckDuckGo";
        in {
          # Don't show first run settings
          "app.normandy.first_run" = false;
          "browser.aboutwelcome.enabled" = false;
          # Enable userChrome styling
          "toolkit.legacyUserProfileCustomizations.stylesheets" = true;
          # Enable extensions to be installed by Nix
          "extensions.autoDisableScopes" = 0;
          
          # Set content blocking to strict
          "browser.contentblocking.category" = "standard";
          # Disable firefox bloatware
          "extensions.pocket.enabled" = false;
          "identity.fxaccounts.enabled" = false;
          "pdfjs.disabled" = true;
          # Always ask where to download a file
          "browser.download.useDownloadDir" = false;
          # Turn off the option to use PDF.js
          "browser.helperApps.showOpenOptionForPdfJS" = false;
          # Turn off auto-updating and let Nix manage updates instead
          "app.update.auto" = false;

          # Use dark theme by default
          "extensions.activeThemeID" = "firefox-compact-dark@mozilla.org";
          # Set homepage to nightTab
          "browser.startup.homepage" = "moz-extension://cd19d231-2b68-45e3-994d-27e68467c0aa/index.html";
          # Allow new tabs to be controlled by an extension
          "browser.newtab.extensionControlled" = true;
          # Allow the extension-based new tab to function in a private tab
          "browser.newtab.privateAllowed" = true;
          # Set UI customization
          "browser.uiCustomization.state" = ''{"placements":{"widget-overflow-fixed-list":["woop-noopscoopsnsxq_jetpack-browser-action","firenvim_lacamb_re-browser-action","_48748554-4c01-49e8-94af-79662bf34d50_-browser-action","_e4a8a97b-f2ed-450b-b12d-ee082ba24781_-browser-action","_7a7a4a92-a2a0-41d1-9fd7-1e92480d612d_-browser-action","ipfs-firefox-addon_lidel_org-browser-action","_c2c003ee-bd69-42a2-b0e9-6f34222cb046_-browser-action","wallet_taler_net-browser-action"],"nav-bar":["back-button","forward-button","stop-reload-button","customizableui-special-spring1","save-to-pocket-button","preferences-button","developer-button","library-button","_testpilot-containers-browser-action","downloads-button","customizableui-special-spring2","urlbar-container","new-tab-button","customizableui-special-spring8","adnauseam_rednoise_org-browser-action","sponsorblocker_ajay_app-browser-action","_73a6fe31-595d-460b-a920-fcc0f8843232_-browser-action","addon_darkreader_org-browser-action","_446900e4-71c2-419f-a6a7-df9c091e268b_-browser-action","_2e5ff8c8-32fe-46d0-9fc8-6b8986621f3c_-browser-action","_74145f27-f039-47ce-a470-a662b129930a_-browser-action","_bd5c0ed8-530d-4325-842a-4f560b3ca4ba_-browser-action","jid1-bofifl9vbdl2zq_jetpack-browser-action","_e7476172-097c-4b77-b56e-f56a894adca9_-browser-action"],"toolbar-menubar":["menubar-items"],"TabsToolbar":["tabbrowser-tabs","alltabs-button"],"PersonalToolbar":["import-button","personal-bookmarks"]},"seen":["save-to-pocket-button","_testpilot-containers-browser-action","firenvim_lacamb_re-browser-action","woop-noopscoopsnsxq_jetpack-browser-action","_48748554-4c01-49e8-94af-79662bf34d50_-browser-action","_a4c4eda4-fb84-4a84-b4a1-f7c1cbf2a1ad_-browser-action","_983bd86b-9d6f-4394-92b8-63d844c4ce4c_-browser-action","_da2b93f0-35d5-461e-9bc7-6ba10aef1af4_-browser-action","_contain-facebook-browser-action","addon_darkreader_org-browser-action","gsconnect_andyholmes_github_io-browser-action","adnauseam_rednoise_org-browser-action","jid1-kkzogwgsw3ao4q_jetpack-browser-action","ipfs-firefox-addon_lidel_org-browser-action","sponsorblocker_ajay_app-browser-action","_2e5ff8c8-32fe-46d0-9fc8-6b8986621f3c_-browser-action","_529b261b-df0b-4e3b-bf42-07b462da0ee8_-browser-action","_446900e4-71c2-419f-a6a7-df9c091e268b_-browser-action","_73a6fe31-595d-460b-a920-fcc0f8843232_-browser-action","_7a7a4a92-a2a0-41d1-9fd7-1e92480d612d_-browser-action","_74145f27-f039-47ce-a470-a662b129930a_-browser-action","_c607c8df-14a7-4f28-894f-29e8722976af_-browser-action","_e4a8a97b-f2ed-450b-b12d-ee082ba24781_-browser-action","developer-button","chrome-gnome-shell_gnome_org-browser-action","_c2c003ee-bd69-42a2-b0e9-6f34222cb046_-browser-action","_bd5c0ed8-530d-4325-842a-4f560b3ca4ba_-browser-action","jid1-bofifl9vbdl2zq_jetpack-browser-action","_react-devtools-browser-action","wallet_taler_net-browser-action","_e7476172-097c-4b77-b56e-f56a894adca9_-browser-action"],"dirtyAreaCache":["nav-bar","TabsToolbar","PersonalToolbar","widget-overflow-fixed-list","toolbar-menubar"],"currentVersion":17,"newElementCount":13}'';
          # Hide toolbars
          "browser.toolbars.bookmarks.visibility" = "never";
          # Set UI density (spacing between elements)
          "browser.uidensity" = 2;

          # Better touchscreen support
          "dom.w3c.touch_events.enabled" = true;

          # Set the default search engine to Swisscows
          "browser.search.defaultenginename" = searchEngine;
          "browser.search.selectedEngine" = searchEngine;
          "browser.urlbar.placeholderName" = searchEngine;
          "browser.policies.runOncePerModification.setDefaultSearchEngine" = searchEngine;
          # Turn off search suggestions
          "browser.search.suggest.enabled" = false;

          # Set fonts
          "font.default.x-western" = "sans-serif";
          "font.name.sans-serif.x-western" = config.fonts.default.font;
          "font.name.serif.x-western" = config.fonts.serif.font;
          "font.name.monospace.x-western" = config.fonts.monospace.font;

          # Firefox GNOME Theme
          "gnomeTheme.hideSingleTab" = true;
          "gnomeTheme.symbolicTabIcons" = true;
          "gnomeTheme.hideWebrtcIndicator" = true;
          "gnomeTheme.dragWindowHeaderbarButtons" = true;
        };
      };
    };
  };
}
