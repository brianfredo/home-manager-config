{ config, pkgs, lib, ... }:

{
  # Symlink tridactyl configuration file
  home.file.".config/tridactyl/tridactylrc".source = ./tridactylrc;
}
