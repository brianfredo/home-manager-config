{ config, pkgs, opkgs, lib, ... }:

let
  mkLuaConfig = file: "lua << EOF\n" + builtins.readFile file + "\nEOF";
in
{
  programs.neovim = {
    enable = true;
    # Alias `vim` to nvim
    vimAlias = true;
    # Extra vimscript
    extraConfig = builtins.readFile ./config.vim;
    # Extra packages eg. ones that are needed for LSP
    extraPackages = with pkgs; with pkgs.nodePackages; [
      vscode-langservers-extracted
      typescript-language-server
      prettier
      eslint
			svelte-language-server
			vls

      rnix-lsp
      rust-analyzer
      rustfmt

      vim-language-server
			vala-language-server
    ];
    # Python LSP packages
    extraPython3Packages = (ps: with ps; [
      python-lsp-server

      autopep8
      rope
      pyflakes
      pycodestyle
      pydocstyle
    ]);
    # Plugins
    plugins = with pkgs.vimPlugins; with opkgs.vimPlugins; [
      # Better tables in markdown
      # {
      #   plugin = vim-table-mode;
      #   config = builtins.readFile ./pluginConfig/vim-table-mode.vim;
      # }
      # Treesitter, a really good syntax highlighting plugin
      {
        plugin = nvim-treesitter.withPlugins (_: pkgs.tree-sitter.allGrammars);
        config = mkLuaConfig ./pluginConfig/nvim-treesitter.lua;
      }
			# Provides a bunch of different language syntaxes
			vim-polyglot
      # Vim plugin for surrounding like [] or {}
      vim-surround
      # Better cutting in vim
      {
        plugin = vim-cutlass;
        config = builtins.readFile ./pluginConfig/vim-cutlass.vim;
      }
      nord-vim
      # {
      #   plugin = (pkgs.vimPlugins.base16-vim.overrideAttrs (old:
      #     let schemeFile = config.scheme base16-vim;
      #     in { patchPhase = ''cp ${schemeFile} colors/base16-scheme.vim''; }
      #   ));
      #   extraConfig = ''
      #     set termguicolors
      #     colorscheme base16-scheme
      #     set background=dark
      #     let base16colorspace=256
      #   '';
      # }
      # Syntax-based completion for over 600 languages
      {
        plugin = syntaxcomplete;
        config = builtins.readFile ./pluginConfig/syntaxcomplete.vim;
      }
      # Provides really good snippets
      friendly-snippets
      # Amazing file tree
      {
        plugin = nvim-tree-lua;
        config = builtins.readFile ./pluginConfig/nvim-tree-lua.vim;
      }
      # Icons for certain plugins
      nvim-web-devicons
      vim-devicons
      # Git support in vim
      vim-fugitive
      vim-gitgutter
      # Asynchronous goodies
      vim-dispatch
      # Lua-based statusline
      {
        plugin = lualine-nvim;
        config = mkLuaConfig (builtins.readFile (config.scheme {
          template = ./pluginConfig/lualine-nvim.lua;
          extension = ".lua";
        }));
      }
      vim-tpipeline
      # Beautiful bufferline
      {
        plugin = bufferline-nvim;
        config = mkLuaConfig ./pluginConfig/bufferline-nvim.lua;
      }
      # HTML Preview plugin
      {
        plugin = bracey-vim;
        config = builtins.readFile ./pluginConfig/bracey.vim;
      }
      # Really good snippet engine
      {
        plugin = luasnip;
        config = mkLuaConfig ./pluginConfig/luasnip.lua;
      }
      cmp_luasnip
      # Easily configure neovim's built-in LSP
      {
        plugin = nvim-lspconfig;
        config = mkLuaConfig ./pluginConfig/nvim-lspconfig.lua;
      }
      cmp-nvim-lsp
      # Extra goodies for using neovim's built-in LSP
      {
        plugin = lspsaga-nvim;
        config = builtins.readFile ./pluginConfig/lspsaga.vim;
      }
      # Make completions using neovim's built-in LSP
      {
        plugin = nvim-cmp;
        config = mkLuaConfig ./pluginConfig/nvim-cmp.lua;
      }
      cmp-buffer
      cmp-calc
      cmp-emoji
      cmp-latex-symbols
      cmp-nvim-lua
      cmp-path
      cmp-spell
      cmp-treesitter
      # Icons for LSP completions
      lspkind-nvim
      # Extra tools for rust
      {
        plugin = rust-tools-nvim;
        config = mkLuaConfig ./pluginConfig/rust-tools-nvim.lua;
      }
      {
        plugin = crates-nvim;
        config = mkLuaConfig ./pluginConfig/crates-nvim.lua;
      }
      # Automatically pair things like [ with ] and { with }
      {
        plugin = nvim-autopairs;
        config = mkLuaConfig ./pluginConfig/nvim-autopairs.lua;
      }
      # Really nice fuzzy finder
      skim-vim
      # Code formatting front-end (beatifier)
      {
        plugin = neoformat;
        config = builtins.readFile ./pluginConfig/neoformat.vim;
      }
      # Comment stuff out really easily
      {
        plugin = commentary;
        config = builtins.readFile ./pluginConfig/commentary.vim;
      }
      # Focus easily
      goyo
      {
        plugin = limelight-vim;
        config = builtins.readFile ./pluginConfig/limelight.vim;
      }
      # Use vim in the browser
      firenvim
      # Better scrolling
      sexy-scroller
      # Tmux plugins
      vim-tmux-navigator
      vim-tmux
      # .editorconfig support
      editorconfig-nvim
    ];
  };

  # Set neovim as the default editor
  home.sessionVariables = rec {
    EDITOR = "${config.programs.neovim.package}/bin/nvim";
    VISUAL = EDITOR;
  };
}
