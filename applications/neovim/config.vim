if (has("termguicolors"))
	set termguicolors
endif


" Theme
syntax enable
let base16colorspace=256


" Font
set guifont=Fira\ Code,MesloLGS\ NF:h15"


" Vim settings

" Mouse support
set mouse=a
" Enable ruler
set number
" Be able to switch buffers without saving
set hidden
" Wrap
set wrap
" Adjust tabbing
set shiftwidth  =2
set tabstop     =2
set autoindent
set smartindent
set noexpandtab
set cinwords    =["{", ":"]
" Turn on ftplugins
filetype plugin on
" Set nord theme
colorscheme nord


" Custom Functions
 
" Detect Start of Line
function! s:IsAtStartOfLine(mapping)
  let text_before_cursor = getline('.')[0 : col('.')-1]
  let mapping_pattern = '\V' . escape(a:mapping, '\')
  let comment_pattern = '\V' . escape(substitute(&l:commentstring, '%s.*$', '', ''), '\')
  return (text_before_cursor =~? '^' . ('\v(' . comment_pattern . '\v)?') . '\s*\v' . mapping_pattern . '\v$')
endfunction

" Change Font Size
let s:fontsize = 12
function! AdjustFontSize(amount)
  let s:fontsize = s:fontsize+a:amount
  :execute "GuiFont! Consolas:h" . s:fontsize
endfunction


" Mappings

" Set Leader Key
let mapleader = ","
let maplocalleader="\<Space>"

" Basic Mappings
nnoremap <silent> <M-f>           :Neoformat<cr>
nnoremap <silent> <M-g>           :Goyo<cr>
nnoremap <silent> <M-L>           :Limelight<cr>
nnoremap <silent> <Leader><space> :noh<cr>
nnoremap <silent> <Leader>s       :set spell<cr>

" Font Adjusting
" Set Ctrl+Scroll keybinds
noremap  <silent> <expr> <C-ScrollWheelUp>   AdjustFontSize(1)<CR>
noremap  <silent> <expr> <C-ScrollWheelDown> AdjustFontSize(-1)<CR>
inoremap <silent> <expr> <C-ScrollWheelUp>   AdjustFontSize(1)<CR>a
inoremap <silent> <expr> <C-ScrollWheelDown> AdjustFontSize(-1)<CR>a
" Set numpad keybindings
noremap <silent> <expr> <kPlus>  AdjustFontSize(1)
noremap <silent> <expr> <kMinus> AdjustFontSize(-1)

" Table Mode
" inoreabbrev <expr> <bar><bar>
          " \ <SID>IsAtStartOfLine('\|\|') ?
          " \ '<c-o>:TableModeEnable<cr><bar><space><bar><left><left>' : '<bar><bar>'
" inoreabbrev <expr> __
          " \ <SID>IsAtStartOfLine('__') ?
          " \ '<c-o>:silent! TableModeDisable<cr>' : '__'

" Fuzzy finding mappings
nnoremap <silent> <C-P>p  :CocFzfList<cr>
nnoremap <silent> <C-P>w  :Windows<cr>
nnoremap <silent> <C-P>b  :Buffers<cr>
nnoremap <silent> <C-P>f  :Files<cr>
nnoremap <silent> <C-P>g  :Rg<cr>
nnoremap <silent> <C-P>d  :CocFzfList diagnostics<cr>
nnoremap <silent> <C-P>e  :CocFzfList extensions<cr>
nnoremap <silent> <C-P>m  :CocFzfList maps<cr>
nnoremap <silent> <C-P>cc :Commands<cr>
nnoremap <silent> <C-P>cv :CocFzfList vimcommands<cr>
nnoremap <silent> <C-P>s  :CocFzfList sessions<cr>

" Vimleader mappings
" Lspsaga code action
nnoremap <silent> <leader>ca :Lspsaga code_action<CR>
vnoremap <silent> <leader>ca :<C-U>Lspsaga range_code_action<CR>

" Window mappings
" nnoremap <C-J> <C-W><C-J>
" nnoremap <C-K> <C-W><C-K>
" nnoremap <C-L> <C-W><C-L>
" nnoremap <C-H> <C-W><C-H>
" Commented out because vim-tmux-navigator takes
" care of this
" nnoremap -     <C-W>-
" nnoremap +     <C-W>+
" nnoremap =     <C-W>=
" nnoremap <M-,> <C-W><
" nnoremap <M-.> <C-W>>
" Commented out because vim-tmux-resizer takes
" care of this
nnoremap <M-s> <C-W>s
nnoremap <M-v> <C-W>v

" Buffer mappings
" noremap  <silent> <C-T> :enew<cr>
noremap  <silent> <M-w> :q<cr>
noremap  <silent> W     :BufferLinePickClose<cr>
nnoremap <silent> <M-[> :BufferLineCyclePrev<cr>
nnoremap <silent> <M-]> :BufferLineCycleNext<cr>
nnoremap <silent> <M-;> :BufferLineMovePrev<CR>
nnoremap <silent> <M-'> :BufferLineMoveNext<CR>


" Tab mappings
noremap <silent> <M-t> :tabnew<cr>
noremap <silent> <M-{> :tabp<cr>
noremap <silent> <M-W> :tabclose<cr>
noremap <silent> <M-}> :tabn<cr>

" Quit keybinds
noremap <silent> <C-Q> :qa<cr>

" Remove newbie crutches in Command Mode
" cnoremap <Down> <Nop>
" cnoremap <Left> <Nop>
" cnoremap <Right> <Nop>
" cnoremap <Up> <Nop>

" Remove newbie crutches in Insert Mode
" inoremap <Down> <Nop>
" inoremap <Left> <Nop>
" inoremap <Right> <Nop>
" inoremap <Up> <Nop>

" Remove newbie crutches in Normal Mode
" nnoremap <Down> <Nop>
" nnoremap <Left> <Nop>
" nnoremap <Right> <Nop>
" nnoremap <Up> <Nop>

" Remove newbie crutches in Visual Mode
" vnoremap <Down> <Nop>
" vnoremap <Left> <Nop>
" vnoremap <Right> <Nop>
" vnoremap <Up> <Nop>
