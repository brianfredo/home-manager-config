require("bufferline").setup {
	options = {
		numbers = function (opts)
			return string.format('%s%s', opts.id, opts.raise(opts.ordinal))
		end,
		middle_mouse_command = "bdelete! %d",
		separator_style = "slant",
		diagnostics = "nvim_lsp",
		offsets = {{
			filetype = "NvimTree",
			highlight = "Directory",
			text = "File Tree",
			text_align = "center"
		}}
	}
}
