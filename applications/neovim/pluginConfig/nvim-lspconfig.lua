local lspconfig = require 'lspconfig'

-- Enable (broadcasting) snippet capability for completion
local capabilities = require 'cmp_nvim_lsp'.update_capabilities(vim.lsp.protocol.make_client_capabilities())
capabilities.textDocument.completion.completionItem.snippetSupport = true
local cap_args = {
	capabilities = capabilities,
}

-- LSPs

-- Use a loop to conveniently call 'setup' on multiple servers and
-- map buffer local keybindings when the language server attaches
local servers = { 'html', 'cssls', 'tsserver', 'jsonls', 'svelte', 'vuels', 'rnix', 'vimls', 'vala_ls', 'gdscript', 'pylsp' }
for _, lsp in pairs(servers) do
  lspconfig[lsp].setup {
    on_attach = on_attach,
  }
end
