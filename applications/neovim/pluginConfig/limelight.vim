" Gray text opacity
let g:limelight_default_coefficient = 0.4
" Paragraph span
let g:limelight_paragraph_span = 1
