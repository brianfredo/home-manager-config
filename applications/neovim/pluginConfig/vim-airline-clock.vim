" Adjust time format
let g:airline#extensions#clock#format = '%H:%M:%S'
" Adjust clock update time
let g:airline#extensions#clock#updatetime = 500
