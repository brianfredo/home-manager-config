" Use One Dark theme
let s:scheme_slug = substitute("{{scheme-slug}}", "-", "_", "g")
let g:airline_theme="base16_". s:scheme_slug
" Use tabline
let g:airline#extensions#tabline#enabled = 1
" Use powerline font
let g:airline_powerline_fonts = 1
" Don't ignore terminals
let g:airline#extensions#tabline#ignore_bufadd_pat =
  \ 'gundo|undotree|vimfiler|tagbar|nerd_tree|startify|!'
