vim.o.completeopt = 'menuone,noselect'

local has_words_before = function()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

-- Setup nvim-cmp.
local cmp = require 'cmp'
local luasnip = require 'luasnip'
local lspkind = require 'lspkind'

cmp.setup {
	completion = {
		border = { "╭", "─", "╮", "│", "╯", "─", "╰", "│" }, -- rounded corners
		scrollbar = "║", -- pretty scrollbar
	},
	window = {
		documentation = { border = "rounded", scrollbar = "║" },
		completion = { border = "rounded", scrollbar = "║" },
	},
	formatting = {
		format = lspkind.cmp_format({
			mode = "symbol_text", -- ordering
			preset = "default", -- font
			
			menu = ({
				nvim_lsp = "[LSP]",
				luasnip = "[Snippet]",
				buffer = "[Buffer]",
				calc = "[Calc]",
				crates = "[Crates]",
				emoji = "[Emoji]",
				latex_symbols = "[LaTeX]",
				path = "[Path]",
				spell = "[Spell]",
				treesitter = "[TS]"
			})
		}),
	},
	snippet = {
		expand = function(args)
			-- For `luasnip` user.
			luasnip.lsp_expand(args.body)
		end,
	},
	mapping = {
    ["<C-p>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, { "i", "s" }),
    ["<C-n>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      elseif has_words_before() then
        cmp.complete()
      else
        fallback()
      end
    end, { "i", "s" }),
    ['<C-Space>'] = cmp.mapping.complete(),
		['<C-e>'] = cmp.mapping({
			i = cmp.mapping.abort(),
			c = cmp.mapping.close(),
		}),
		['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
	},
	sources = {
		{ name = 'nvim_lsp' },

		-- For luasnip user.
		{ name = 'luasnip' },

		{ name = 'buffer' },
		{ name = 'calc' },
		{ name = "crates" },
		{ name = 'emoji' },
		{ name = 'latex_symbols' },
		{ name = 'path' },
		{ name = 'spell' },
		{ name = 'treesitter' },
	}
}

cmp.setup.cmdline(':', {
  sources = {
    { name = 'cmdline' }
  }
})

cmp.setup.cmdline('/', {
  sources = {
    { name = 'buffer' }
  }
})

-- If you want insert `(` after select function or method item
local cmp_autopairs = require('nvim-autopairs.completion.cmp')
cmp.event:on( 'confirm_done', cmp_autopairs.on_confirm_done({ map_char = { tex = '' } }))
