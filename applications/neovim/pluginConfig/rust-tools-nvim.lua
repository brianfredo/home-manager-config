require('rust-tools').setup({
	runnables = {
			-- whether to use telescope for selection menu or not
			use_telescope = false

			-- rest of the opts are forwarded to telescope
	},

	debuggables = {
			-- whether to use telescope for selection menu or not
			use_telescope = false

			-- rest of the opts are forwarded to telescope
	},
})
