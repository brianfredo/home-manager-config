" Try formatprg
let g:neoformat_try_formatprg = 1
" Enable basic alignment even when no formatters are available
let g:neoformat_basic_format_align = 1
" Enable trimmming of trailing whitespace in all formatters
let g:neoformat_basic_format_trim = 1
