lua << EOF
require'nvim-tree'.setup {
	hijack_cursor = true,
}
EOF

let g:nvim_tree_ignore = [ '.git', 'node_modules', '.cache' ]
let g:nvim_tree_gitignore = 1
let g:nvim_tree_git_hl = 1 " will enable file highlight for git attributes (can be used without the icons).
let g:nvim_tree_highlight_opened_files = 1 " will enable folder and file icon highlight for opened files/directories.
let g:nvim_tree_hijack_netrw = 0 " prevents netrw from automatically opening when opening directories (but lets you keep its other utilities)
let g:nvim_tree_group_empty = 1 " compact folders that only contain a single folder into one node in the file tree
let g:nvim_tree_lsp_diagnostics = 1 " will show lsp diagnostics in the signcolumn. See :help nvim_tree_lsp_diagnostics
let g:nvim_tree_update_cwd = 1 " will update the tree cwd when changing nvim's directory (DirChanged event). Behaves strangely with autochdir set.
let g:nvim_tree_window_picker_exclude = {
    \   "filetype": [
    \     "minimap"
    \   ],
    \ }
let g:nvim_tree_special_files = {}
let g:nvim_tree_show_icons = {
    \   "git": 1,
    \   "folders": 1,
    \   "files": 1,
    \   "folder_arrows": 1,
		\ }
let g:nvim_tree_icons = {
		\ "folder": {
		\   "arrow_open": "▾",
		\   "arrow_closed": "▸",
		\ }}

nnoremap <C-t> :NvimTreeToggle<CR>
nnoremap <leader>r :NvimTreeRefresh<CR>

highlight NvimTreeFolderIcon guibg=blue
