" Make tables markdown compatible
let g:table_mode_corner='|'
" Add hooks to entering table-mode
function! TableModeEnabled()
	set nowrap
endfunction
autocmd User TableModeEnabled call TableModeEnabled()
" Add hooks to exiting table-mode
function! TableModeDisabled()
	set wrap
endfunction
autocmd User TableModeDisabled call TableModeDisabled()
